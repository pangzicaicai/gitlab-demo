from flask import Flask


app = Flask(__name__)


@app.route("/echo/<string:echo_str>", methods=['GET'])
def echo(echo_str):
    return echo_str


if __name__ == '__main__':
    app.run()

